package com.webringcatharsis.tripd;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;

/**
 * Created by SYUS on 12/24/2015 at 3:50 PM for r0457206_Tripd
 */
public class tripdLocationManager implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private String mLatitudeString;
    private String mLongitudeString;
    private String formattedEstAddress;

    private GoogleApiClient mGoogleApiClient;
    private Geocoder geocoder;
    private Location mLastLocation;

    private Context applicationContext;

    public tripdLocationManager(Context applicationContext) {
        this.applicationContext = applicationContext;
        mGoogleApiClient = new GoogleApiClient.Builder(applicationContext)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    public String getmLatitudeString() {
        return mLatitudeString;
    }

    public void setmLatitudeString(String mLatitudeString) {
        this.mLatitudeString = mLatitudeString;
    }

    public String getmLongitudeString() {
        return mLongitudeString;
    }

    public void setmLongitudeString(String mLongitudeString) {
        this.mLongitudeString = mLongitudeString;
    }

    public String getFormattedEstAddress() {
        return formattedEstAddress;
    }

    public void setFormattedEstAddress(String formattedEstAddress) {
        this.formattedEstAddress = formattedEstAddress;
    }

    public String getGoogleMapsQuickLink() {
        return applicationContext.getString(R.string.tripd_google_maps_quicklink_url) + getmLatitudeString() + "," + getmLongitudeString();
    }

    @Override
    public void onConnected(Bundle bundle) {
        updateLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        updateLocation();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public void start() {
        mGoogleApiClient.connect();
    }

    public void close() {
        mGoogleApiClient.disconnect();
    }

    public void updateLocation() {
        Log.d(applicationContext.getString(R.string.tripr_name), "updating location");
            try {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if (mLastLocation != null) {
                    setmLatitudeString(String.valueOf(mLastLocation.getLatitude()));
                    setmLongitudeString(String.valueOf(mLastLocation.getLongitude()));
                    getEstAddressFromLocation();
                }
                Log.d(applicationContext.getString(R.string.tripr_name), "location is " + mLastLocation.toString());
            } catch (SecurityException se) {
                //i was checking permissions properly, but IDE was still complaining so catching
                Log.e(applicationContext.getString(R.string.tripr_name), "Don't have permission to accesss fine location", se);
            }
    }

    private void getEstAddressFromLocation() {
        geocoder = new Geocoder(applicationContext);
        setFormattedEstAddress("");
        try {
            List<Address> addresses = geocoder.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 10); //<10>
            for (Address address : addresses) {
                setFormattedEstAddress(getFormattedEstAddress() + " " + address.getAddressLine(0) + "\n");
            }
        } catch (IOException e) {
            Log.e(applicationContext.getString(R.string.tripr_name), "Could not get Geocoder data", e);
        }
    }
}
