package com.webringcatharsis.tripd;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


public class tripdMainActivity extends AppCompatActivity {

    private ToggleButton toggleServiceButton;

    private Button selectContact1Button;
    private Button selectContact2Button;
    private Button selectContact3Button;

    private TextView contact1TextView;
    private TextView contact2TextView;
    private TextView contact3TextView;

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tripr__main);
        init();
    }

    private void init() {
        sharedPreferences = this.getSharedPreferences(tripdConstants.DATA.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);

        contact1TextView = (TextView)findViewById(R.id.contact1TextView);
        contact2TextView = (TextView)findViewById(R.id.contact2TextView);
        contact3TextView = (TextView)findViewById(R.id.contact3TextView);

        contact1TextView.setText(sharedPreferences.getString(tripdConstants.DATA.PHONE_NO_1, "ICE 1"));
        contact2TextView.setText(sharedPreferences.getString(tripdConstants.DATA.PHONE_NO_2, "ICE 2"));
        contact3TextView.setText(sharedPreferences.getString(tripdConstants.DATA.PHONE_NO_3, "ICE 3"));

        selectContact1Button = (Button)findViewById(R.id.selectContact1Button);
        selectContact1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(i, tripdConstants.ID.CONTACT_PICKER_1);
            }
        });

        selectContact2Button = (Button)findViewById(R.id.selectContact2Button);
        selectContact2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(i, tripdConstants.ID.CONTACT_PICKER_2);
            }
        });

        selectContact3Button = (Button)findViewById(R.id.selectContact3Button);
        selectContact3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(i, tripdConstants.ID.CONTACT_PICKER_3);
            }
        });

        toggleServiceButton = (ToggleButton)findViewById(R.id.toggleServiceButton);
        toggleServiceButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    startService();
                } else {
                    stopService();
                }
            }
        });

        if(isServiceRunning()) {
            toggleServiceButton.setChecked(true);
        }
    }

    //it's bad to rely on a sharedpref boolean that is not set in the service itself
    //but this seemed the cleanest way to detect whether or not the service was running
    private void setServiceRunning(boolean running) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(tripdConstants.DATA.SERVICE_STATE, running);
        editor.apply();
    }

    public boolean isServiceRunning() {
        return sharedPreferences.getBoolean(tripdConstants.DATA.SERVICE_STATE, false);
    }

    private void startService() {
        Intent startIntent = new Intent(tripdMainActivity.this, tripdService.class);
        startIntent.setAction(tripdConstants.ACTION.STARTFOREGROUND_ACTION);
        startService(startIntent);
        setServiceRunning(true);
    }

    private void stopService() {
        Intent startIntent = new Intent(tripdMainActivity.this, tripdService.class);
        startIntent.setAction(tripdConstants.ACTION.STOPFOREGROUND_ACTION);
        startService(startIntent);
        setServiceRunning(false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if(intent != null) {
            String phoneNo = "";
            Uri uri = intent.getData();

            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();

            int  phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            phoneNo = cursor.getString(phoneIndex);
            cursor.close();

            SharedPreferences.Editor editor = sharedPreferences.edit();

            switch (requestCode) {
                case tripdConstants.ID.CONTACT_PICKER_1:
                    editor.putString(tripdConstants.DATA.PHONE_NO_1, phoneNo);
                    editor.commit();
                    contact1TextView.setText(sharedPreferences.getString(tripdConstants.DATA.PHONE_NO_1, "ICE 1"));
                    Toast.makeText(this, "committed phone no 1", Toast.LENGTH_SHORT).show();
                    break;
                case tripdConstants.ID.CONTACT_PICKER_2:
                    editor.putString(tripdConstants.DATA.PHONE_NO_2, phoneNo);
                    editor.commit();
                    contact2TextView.setText(sharedPreferences.getString(tripdConstants.DATA.PHONE_NO_2, "ICE 2"));
                    Toast.makeText(this, "committed phone no 2", Toast.LENGTH_SHORT).show();
                    break;
                case tripdConstants.ID.CONTACT_PICKER_3:
                    editor.putString(tripdConstants.DATA.PHONE_NO_3, phoneNo);
                    editor.commit();
                    contact3TextView.setText(sharedPreferences.getString(tripdConstants.DATA.PHONE_NO_3, "ICE 3"));
                    Toast.makeText(this, "committed phone no 3", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_tripr__main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
