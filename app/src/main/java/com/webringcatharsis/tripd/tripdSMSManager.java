package com.webringcatharsis.tripd;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.SmsManager;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by SYUS on 12/24/2015 at 4:57 PM for r0457206_Tripd
 */
public class tripdSMSManager {
    private Context applicationContext;

    public tripdSMSManager(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void contactICEs(String googleMapsQuickLink, String formattedEstAddress) {
        Log.d(applicationContext.getString(R.string.tripr_name), "Sending SMS");
        SharedPreferences sharedPreferences = applicationContext.getSharedPreferences(tripdConstants.DATA.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String phoneNo1 = sharedPreferences.getString(tripdConstants.DATA.PHONE_NO_1, null);
        String phoneNo2 = sharedPreferences.getString(tripdConstants.DATA.PHONE_NO_2, null);
        String phoneNo3 = sharedPreferences.getString(tripdConstants.DATA.PHONE_NO_3, null);

        String formattedSms = getFormattedSms(googleMapsQuickLink, formattedEstAddress);
        try {
            SmsManager smsManager = SmsManager.getDefault();
            ArrayList<String> messagelist = smsManager.divideMessage(formattedSms);

            if(phoneNo1 != null) {
                smsManager.sendMultipartTextMessage(phoneNo1, null, messagelist, null, null);
            }
            if(phoneNo2 != null) {
                smsManager.sendMultipartTextMessage(phoneNo2, null, messagelist, null, null);
            }
            if(phoneNo3 != null) {
                smsManager.sendMultipartTextMessage(phoneNo3, null, messagelist, null, null);
            }

            Log.d(applicationContext.getString(R.string.tripr_name), "SMS sent");
            Log.d(applicationContext.getString(R.string.tripr_name), "Content: " + messagelist);
        } catch (Exception e) {
            Log.d(applicationContext.getString(R.string.tripr_name), "Failed to send SMS");
            e.printStackTrace();
        }
    }

    private String getFormattedSms(String googleMapsQuickLink, String formattedEstAddress) {
        String SMSMessage = applicationContext.getString(R.string.tripr_sms_message_line1)
                + applicationContext.getString(R.string.tripr_sms_message_line2) + googleMapsQuickLink + "\n"
                + applicationContext.getString(R.string.tripr_sms_message_line3) + formattedEstAddress + "\n"
                + applicationContext.getString(R.string.tripr_sms_message_line4);
        return SMSMessage;
    }

}
