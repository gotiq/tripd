package com.webringcatharsis.tripd;

/**
 * Created by SYUS on 12/26/2015 at 4:19 PM for r0457206_Tripd
 */
public class tripdConstants {
    public interface ACTION {
        String STARTFOREGROUND_ACTION = "com.webringcatharsis.tripd.action.startforeground";
        String STOPFOREGROUND_ACTION = "com.webringcatharsis.tripd.action.stopforeground";
        String GREENLIGHT_ACTION = "com.webringcatharsis.tripd.action.cliengreenlight";
    }

    public interface ID {
        int CONTACTING_ICES = 9001;
        int TRIP_NOTIFICATION = 9002;
        int SERVICE_ACTIVE = 9003;
        int CONTACT_PICKER_1 = 9004;
        int CONTACT_PICKER_2 = 9005;
        int CONTACT_PICKER_3 = 9006;
    }

    public interface DATA {
        String SHARED_PREFERENCES_NAME = "com.webringcatharsis.tripd.name.prefs";
        String SERVICE_STATE = "com.webringcatharsis.tripd.name.servicestate";
        String PHONE_NO_1 = "com.webringcatharsis.tripd.name.phoneno1";
        String PHONE_NO_2 = "com.webringcatharsis.tripd.name.phoneno2";
        String PHONE_NO_3 = "com.webringcatharsis.tripd.name.phoneno3";
    }
}