package com.webringcatharsis.tripd;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * Created by SYUS on 12/25/2015 at 4:39 PM for r0457206_Tripd
 */
public class tripdNotificationManager {
    private Context applicationContext;

    private Notification.Builder contactingICEsNotificationBuilder;
    private Notification.Builder tripNotificationBuilder;

    private NotificationManager notificationManager;

    public tripdNotificationManager(Context applicationContext) {
        this.applicationContext = applicationContext;
        notificationManager = (NotificationManager) applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);
        init();
    }

    private void init() {
        Intent intent = new Intent(applicationContext, tripdService.class);
        intent.setAction(tripdConstants.ACTION.GREENLIGHT_ACTION);
        PendingIntent pIntent = PendingIntent.getService(applicationContext, 0, intent, 0);

        tripNotificationBuilder = new Notification.Builder(applicationContext)
                .setContentTitle(applicationContext.getString(R.string.tripr_message_title))
                .setContentText(applicationContext.getString(R.string.tripr_message) + 60 + applicationContext.getString(R.string.tripr_message_end))
                .setSmallIcon(R.drawable.common_plus_signin_btn_icon_dark)
                .addAction(R.drawable.common_plus_signin_btn_icon_dark, applicationContext.getString(R.string.tripr_action) , pIntent)
                .setPriority(Notification.PRIORITY_HIGH)
                .setVibrate(new long [0]);//cheaty, but a heads up notif requires a vibration pattern or a sound afaik

        contactingICEsNotificationBuilder = new Notification.Builder(applicationContext)
                .setContentTitle(applicationContext.getString(R.string.tripr_countdown_finished_title))
                .setContentText(applicationContext.getString(R.string.tripr_countdown_finished))
                .setSmallIcon(R.drawable.common_plus_signin_btn_icon_dark)
                .setPriority(Notification.PRIORITY_HIGH)
                .setVibrate(new long [0]);//cheaty, but a heads up notif requires a vibration pattern or a sound afaik
    }

    public void cancelTripNotification() {
        notificationManager.cancel(tripdConstants.ID.TRIP_NOTIFICATION);
    }

    public void updateTripNotificationText(long remaining) {
        tripNotificationBuilder.setContentText(applicationContext.getString(R.string.tripr_message) + remaining + applicationContext.getString(R.string.tripr_message_end));
        showOrUpdateTripNotification();
    }

    public void showOrUpdateTripNotification() {
        notificationManager.notify(tripdConstants.ID.TRIP_NOTIFICATION, tripNotificationBuilder.build());
    }

    public void showContactingICEsNotification() {
        notificationManager.notify(tripdConstants.ID.CONTACTING_ICES, contactingICEsNotificationBuilder.build());
    }

    public void cancelAllNotifications() {
        notificationManager.cancelAll();
    }
}
