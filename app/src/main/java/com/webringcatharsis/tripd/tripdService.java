package com.webringcatharsis.tripd;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;


/**
 * Created by SYUS on 12/26/2015 at 3:49 PM for r0457206_Tripd
 */
public class tripdService extends Service implements SensorEventListener {

    // This decides how hard the device has to shake before triggering countdown
    // Do note the current value is for debugging, a realistic value would be much higher
    private static final int SHAKE_THRESHOLD = 1600;//a value from 3200 to 6400 seems reasonably realistic
    private static final long TIMER_MAX = 30000;
    private static final long TIMER_INTERVAL = 1000;
    private static final long VIBRATE_DEVICE_LENGTH = 128;

    private long lastUpdate = 0;
    private float last_x, last_y, last_z;

    private SensorManager senSensorManager;
    private Sensor senAccelerometer;

    private tripdLocationManager tLocationManager;
    private tripdNotificationManager tNotificationManager;
    private tripdSMSManager tSMSManager;

    private CountDownTimer countDown;

    @Override
    public void onCreate() {
        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        tLocationManager = new tripdLocationManager(getApplicationContext());
        tNotificationManager = new tripdNotificationManager(getApplicationContext());
        tSMSManager = new tripdSMSManager(getApplicationContext());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        switch (intent.getAction()) {
            case tripdConstants.ACTION.STARTFOREGROUND_ACTION:
                Log.d(this.getString(R.string.tripr_name), "Trying to start service");
                initService();
                tLocationManager.start();
                initCountDown();
                break;
            case tripdConstants.ACTION.STOPFOREGROUND_ACTION:
                senSensorManager.unregisterListener(this);
                tLocationManager.close();
                stopForeground(true);
                stopSelf();
                Log.d(this.getString(R.string.tripr_name), "Stopped service");
                break;
            case tripdConstants.ACTION.GREENLIGHT_ACTION:
                Log.d(this.getString(R.string.tripr_name), "User okay intent received");
                countDown.cancel();
                tNotificationManager.cancelAllNotifications();
                registerSensor();
            default:
        }
        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    private void initService() {
        Intent notificationIntent = new Intent(this, tripdMainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification.Builder activeNotificationBuilder = new Notification.Builder(this)
                .setContentTitle(this.getString(R.string.tripr_active))
                .setContentText(this.getString(R.string.tripr_active_message))
                .setSmallIcon(R.drawable.common_plus_signin_btn_icon_dark)
                .setOngoing(true)
                .setContentIntent(pendingIntent);

        startForeground(tripdConstants.ID.SERVICE_ACTIVE, activeNotificationBuilder.build());
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            long curTime = System.currentTimeMillis();

            if ((curTime - lastUpdate) > 100) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;

                float speed = Math.abs(x + y + z - last_x - last_y - last_z)/ diffTime * 10000;

                if (speed > SHAKE_THRESHOLD) {
                    Log.d(this.getString(R.string.tripr_name), "Detected mishap");
                    Toast.makeText(this, "Detected mishap", Toast.LENGTH_SHORT).show();
                    tLocationManager.updateLocation();
                    senSensorManager.unregisterListener(this);//prevent multiple detections as long as countdown runs
                    countDown.start();
                }
                last_x = x;
                last_y = y;
                last_z = z;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void vibrateDevice() {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(VIBRATE_DEVICE_LENGTH);
    }

    private void initCountDown() {
        countDown = new CountDownTimer(TIMER_MAX, TIMER_INTERVAL) {
            public void onTick(long millisUntilFinished) {
                tNotificationManager.updateTripNotificationText(millisUntilFinished / 1000);
                vibrateDevice();
            }

            public void onFinish() {
                tNotificationManager.cancelTripNotification();
                tSMSManager.contactICEs(tLocationManager.getGoogleMapsQuickLink(), tLocationManager.getFormattedEstAddress());
                tNotificationManager.showContactingICEsNotification();
                registerSensor();
            }
        };
    }

    private void registerSensor() {
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }
}